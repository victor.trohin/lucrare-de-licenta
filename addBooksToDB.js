import fs from 'fs';
import { parse } from '@fast-csv/parse';
import fetch from 'node-fetch';
import axios from 'axios';
import Book from './models/Book.js';

const options = {
    objectMode: true,
    delimiter: ",",
    quote: null,
    headers: true,
    renameHeaders: false,
  };

async function getFileContentFromURL(url) {
  try {
    const response = await fetch(url);
    const fileContent = await response.text();
    return fileContent;
  } catch (error) {
    console.error('Error fetching book content(text):', error);
    return null;
  }
}

async function getImageFromURL(url) {
  try {
    const response = await axios.get(url, { responseType: 'arraybuffer' });
    const imageData = Buffer.from(response.data, 'binary');
    return imageData;
  } catch (error) {
    console.error('Error saving image to the book:', error);
    return null;
  }
}

const addBooksToDB = async () => {
  fs.createReadStream('./book-metadata.csv')
    .pipe(parse(options))
    .on('error', (error) => {
      console.log(error);
    })
    .on('data', async (row) => {
      const id = row['Text#'];

      const fileContentPromise = getFileContentFromURL(
        `https://www.gutenberg.org/cache/epub/${id}/pg${id}.txt`
      );
      const imageContentPromise = getImageFromURL(
        `https://www.gutenberg.org/cache/epub/${id}/pg${id}.cover.medium.jpg`
      );

      const fileContent = await fileContentPromise;
      const imageContent = await imageContentPromise;

      const book = new Book({
        book_id : id,
        book_title : row['Title'],
        book_language : row['Language'],
        book_authors : !row['Authors']? 'Unknown': row['Authors'],
        book_subjects : !row['Subjects']? 'Unknown': row['Subjects'],
        book_shelves : !row['Bookshelves']? 'Unknown': row['Bookshelves'],
        book_image : imageContent,
        book_text : fileContent,
    });
    try {
        // Save the book to the database
        await book.save();
        console.log(`Book with ID: ${book.book_id} and Title: ${book.book_title} saved to database`);
      } catch (err) {
        console.error(err);
      }
    })
    .on('end', (rowCount) => {
      console.log(`Processed ${rowCount} books`);
    });
};

addBooksToDB();
