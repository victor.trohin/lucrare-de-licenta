import User from "../models/User.js"
import { StatusCodes } from "http-status-codes"
import BadRequestError from "../errors/bad-request.js";
import UnauthenticatedError from "../errors/unauthenticated.js";

const register = async (req, res) => {
    const {name, email, password } = req.body;
    if(!name || !email || !password){
        throw new BadRequestError('please provide all values');
    }

    const userAlreadyExists = await User.findOne({email});
    if(userAlreadyExists){
        throw new BadRequestError('The email already exists')
    }

    const user = await User.create(req.body)
    const token = user.createJWT()
    res.status(StatusCodes.CREATED).json({
        user:{
            email: user.email,
            name: user.name,
            lastName: user.lastName,
            location: user.location,
            favourites_list: user.favourites_list,
        }, 
        token,
        location: user.location,
    })
}

const login = async (req, res) => {
    const {email, password} = req.body
    if(!email || !password){
        throw new BadRequestError("Please provide all the values")
    }
    const user = await User.findOne({email}).select('+password')

    if(!user){
        throw new UnauthenticatedError("Invalid credentials");
    }
    
    const isPasswordCorrect = await user.comparePassword(password);
    if(!isPasswordCorrect){
        throw new UnauthenticatedError("Invalid credentials");
    }
    const token = user.createJWT();
    user.password = undefined
    res.status(StatusCodes.OK).json({user, token, location: user.location})
}

const updateUser = async (req, res) => {
  const { email, name, lastName, location } = req.body;
  if (!email || !name || !lastName || !location) {
    throw new BadRequestError('Please provide all values');
  }

  const updatedUser = await User.findByIdAndUpdate(
    req.user.userId,
    { email, name, lastName, location },
    { new: true, runValidators: true }
  );

  if (!updatedUser) {
    throw new BadRequestError('User not found');
  }

  const token = updatedUser.createJWT();
  res.status(StatusCodes.OK).json({
    user: updatedUser,
    token,
    location: updatedUser.location,
  });
};

const getFavourites = async (req, res) => {
  res.send('NOT IMPLEMENTED: get favourite books');
}

const addFavourites = async (req, res) => {
  const { email, book_id } = req.body;
  const user = await User.findOne({email});
  const {favourites_list} = user;

  if(favourites_list.includes(book_id)){
    throw new BadRequestError('Book already exists in favourites');
  }
  favourites_list.push(book_id)
  const updatedUser = await User.findByIdAndUpdate(
    user._id,
    { favourites_list: favourites_list },
    { new: true, runValidators: true }
  );

  if (!updatedUser) {
    throw new BadRequestError('User not found');
  }
  res.status(StatusCodes.OK).json({
    favourites_list: favourites_list,
  });
}

const removeFromFavourites = async (req, res) => {
  const { email, book_id } = req.body;
  const user = await User.findOne({email});
  const {favourites_list} = user;
  const index = favourites_list.indexOf(book_id);

  if (index === -1) {
    throw new BadRequestError('Book not in favourites list!');
  }
  favourites_list.splice(index, 1);
  
  const updatedUser = await User.findByIdAndUpdate(
    user._id,
    { favourites_list: favourites_list },
    { new: true, runValidators: true }
  );

  if (!updatedUser) {
    throw new BadRequestError('User not found');
  }
  res.status(StatusCodes.OK).json({
    favourites_list: favourites_list,
  });
}

const addTranslatedToList = async (req, res) => {
  const { email, book_id, translatedLanguage } = req.body;
  const user = await User.findOne({email});
  const {translated_list} = user;
  let isMatchingBook = false;
  if(translated_list.length !== 0){
    isMatchingBook = translated_list.some(book =>
      book.book_id === book_id && book.language === translatedLanguage
    );
  }
  if(isMatchingBook){
    throw new BadRequestError('Book already exists in translated list');
  }
  translated_list.push({book_id: book_id, language: translatedLanguage})
  const updatedUser = await User.findByIdAndUpdate(
    user._id,
    { translated_list: translated_list },
    { new: true, runValidators: true }
  );

  if (!updatedUser) {
    throw new BadRequestError('User not found');
  }
  res.status(StatusCodes.OK).json({
    translated_list: translated_list,
  });
}

const addBookToLibrary = async (req, res) => {
  const { email, book_id, language } = req.body;
  const user = await User.findOne({email});
  const {library_list} = user;
  let isMatchingBook = false;
  if(library_list.length !== 0){
    isMatchingBook = library_list.some(book =>
      book.book_id === book_id && book.language === language
    );
  }
  if(isMatchingBook){
    throw new BadRequestError('Book already exists in library');
  }
  library_list.push({book_id: book_id, language: language, progress: 0})
  const updatedUser = await User.findByIdAndUpdate(
    user._id,
    { library_list: library_list },
    { new: true, runValidators: true }
  );

  if (!updatedUser) {
    throw new BadRequestError('User not found');
  }
  res.status(StatusCodes.OK).json({
    library_list: library_list,
  });
}

const addReadProgress = async (req, res) => {
  const { email, book_id, language, progress } = req.body;
  const user = await User.findOne({ email });
  const { library_list } = user;
  let isMatchingBook = false;

  if (library_list.length !== 0) {
    for (let i = 0; i < library_list.length; i++) {
      const book = library_list[i];
      if (book.book_id === book_id && book.language === language) {
        book.progress = book.progress + Number(progress);
        isMatchingBook = true;
        break;
      }
    }
  }

  if (!isMatchingBook) {
    throw new BadRequestError('Book does not exist in library');
  }

  const updatedUser = await User.findByIdAndUpdate(
    user._id,
    { library_list: library_list },
    { new: true, runValidators: true }
  );

  if (!updatedUser) {
    throw new BadRequestError('User not found');
  }

  res.status(StatusCodes.OK).json({
    library_list: library_list,
  });
};

export {
  register, 
  login, 
  updateUser, 
  getFavourites, 
  addFavourites, 
  removeFromFavourites, 
  addTranslatedToList, 
  addBookToLibrary,
  addReadProgress,
}