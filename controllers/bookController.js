import Book from "../models/Book.js"
import TranslatedBook from "../models/TranslatedBook.js";
import { StatusCodes } from "http-status-codes"
import fs from 'fs';
import { parse } from '@fast-csv/parse';
import fetch from 'node-fetch';
import axios from 'axios';
import { Configuration, OpenAIApi } from 'openai'
import BadRequestError from "../errors/bad-request.js";
import { wss } from "../server.js";
import WebSocket from "ws";
const configuration = new Configuration({
  apiKey: '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',
});

const options = {
    objectMode: true,
    delimiter: ",",
    quote: null,
    headers: true,
    renameHeaders: false,
  };

  

const getAllBooks = async(req, res, next) => {
    try {
        const books = await Book.find({}, '-book_text');
        res.status(StatusCodes.OK).json({books});
      } catch (error) {
        next(error);
      }
}

const getBookById = async(req, res) => {
  const { bookId } = req.params;
  try {
    const book = await Book.findOne({ book_id: bookId }, '-book_text'); // Retrieve only book_text field
    res.send(book);
  } catch (error) {
    console.error('Book not found:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

const getBookIdText = async (req, res) => {
  const { bookId } = req.params;
  try {
    const book = await Book.findOne({ book_id: bookId }, 'book_text'); // Retrieve only book_text field
    if (!book) {
      return res.status(404).json({ msg: 'Book not found' });
    }
    const bookText = book.book_text.toString('utf8');
    res.send(bookText);
  } catch (error) {
    console.error('Error fetching book text:', error);
    res.status(500).json({ msg: 'Internal server error' });
  }
}

async function getFileContentFromURL(url) {
  try {
    const response = await fetch(url);
    const fileContent = await response.text();
    return fileContent;
  } catch (error) {
    console.error('Error fetching book content(text):', error);
    return null;
  }
}

async function getImageFromURL(url) {
  try {
    const response = await axios.get(url, { responseType: 'arraybuffer' });
    const imageData = Buffer.from(response.data, 'binary');
    return imageData;
  } catch (error) {
    console.error('Error saving image to the book:', error);
    return null;
  }
}

const addBook = async (req, res) => {
    fs.createReadStream("./book-metadata.csv")
        .pipe(parse(options))
        .on("error", (error) => {
            console.log(error);
        })
        .on("data", async (row) => {
            const id = row['Text#']

            const fileContentPromise = getFileContentFromURL(`https://www.gutenberg.org/cache/epub/${id}/pg${id}.txt`);
            const imageContentPromise = getImageFromURL(`https://www.gutenberg.org/cache/epub/${id}/pg${id}.cover.medium.jpg`);
      
            const fileContent = await fileContentPromise;
            const imageContent = await imageContentPromise;

            const book = new Book({
                book_id : id,
                book_title : row['Title'],
                book_language : row['Language'],
                book_authors : !row['Authors']? 'Unknown': row['Authors'],
                book_subjects : !row['Subjects']? 'Unknown': row['Subjects'],
                book_shelves : !row['Bookshelves']? 'Unknown': row['Bookshelves'],
                book_image : imageContent,
                book_text : fileContent,
            });
            try {
                // Save the book to the database
                await book.save();
                console.log(`Book with ID: ${book.book_id} and Title: ${book.book_title} saved to database`);
              } catch (err) {
                console.error(err);
              }
        })
        .on("end", (rowCount) => {
            console.log(rowCount);
        });

        res.send('Books added to DB')
  };

const updateBook = async (req, res) => {
    res.send('NOT IMPLEMENTED: update book')
}

const deleteBook = async (req, res) => {
    res.send('NOT IMPLEMENTED: delete book')
}

const addBookToFavourites = async (req, res) => {
  res.send('NOT IMPLEMENTED: add to favourites book')
}

function splitIntoChunks(text, wordsPerChunk) {
  const sentences = text.split(/([.?!])/); // Split the text into sentences, including punctuation marks
  let currentChunk = '';
  let currentWordCount = 0;
  const chunks = [];
  sentences.forEach((sentence) => {
    if (/[.?!]/.test(sentence)) {
      // If the sentence is a punctuation mark, add it to the current chunk
      currentChunk += sentence;
    } else {
      const words = sentence.trim().split(' ');
      const sentenceWordCount = words.length;
      if (currentWordCount + sentenceWordCount <= wordsPerChunk) {
        currentChunk += sentence.trim() + ' ';
        currentWordCount += sentenceWordCount;
      } else {
        chunks.push(currentChunk.trim());
        currentChunk = sentence.trim() + ' ';
        currentWordCount = sentenceWordCount;
      }
    }
  });
  // Add the last chunk if it's not empty
  if (currentChunk !== '') {
    chunks.push(currentChunk.trim());
  }

  return chunks;
}

const sendProgressToClients = (progress) => {
  const progressMessage = JSON.stringify({ progress });
  wss.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(progressMessage);
    }
  });
};

async function translateChunks(chunks, targetLanguage, originalLanguage) {
  const translatedChunks = [];
  const translateTo = getLanguageName(targetLanguage);
  const translateFrom = getLanguageName(originalLanguage);
  const openai = new OpenAIApi(configuration);
  for (let i = 0; i < chunks.length; i++) {
    const chunk = chunks[i];
    const prompt = `Translate the following book text from ${translateFrom} language to ${translateTo} language: ${chunk}`
    try {
      const response = await openai.createChatCompletion({
        "model": "gpt-3.5-turbo-16k",
        "messages": [
          {
            "role": "system",
            "content": "You act as a translator, take the text and translate it in the provided language. Also, put the correct punctuation marks."
          },
          {
            "role": "user",
            "content": prompt,
          }
        ]
      });
      const translatedChunk = response.data.choices[0].message.content.trim();
      translatedChunks.push(translatedChunk);
      let progress = Math.round(((i + 1) / chunks.length) * 100);
      sendProgressToClients(progress);
    } catch (error) {
      console.error(error.message);
      throw new BadRequestError("Error translating a chunk");
    }
  }

  return translatedChunks.join(' ');
}

const translateBook = async (req, res) => {
  const { book_text, targetLanguage, originalLanguage, book_id } = req.body;
  try {
    const translatedBook = await TranslatedBook.findOne({ book_id: book_id, translated_language: targetLanguage }, '-book_text');
    if (translatedBook) {
      throw new BadRequestError("Book already translated");
    }
    const targetWordCount = 2000;
    const chunks = splitIntoChunks(book_text, targetWordCount);

    const translatedText = await translateChunks(chunks, targetLanguage, originalLanguage);
    const newTranslatedBook = new TranslatedBook({
      book_id: book_id,
      original_language: originalLanguage,
      translated_language: targetLanguage,
      book_text: translatedText,
    });
    try {
      await newTranslatedBook.save();
      console.log(`Book with ID: ${newTranslatedBook.book_id} saved to the database`);
    } catch (err) {
      console.error(err);
    }
    res.status(StatusCodes.OK).json({ translated_text: translatedText });
  } catch (err) {
    console.error(err);
    res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(err.msg);
  }
};

const findTranslatedBook = async (req, res) => {
  const {book_id, language } = req.body 
  try {
    const translatedBook = await TranslatedBook.findOne({ book_id: book_id, translated_language: language }, '-book_text');
    if(translatedBook){
      console.log(`found translated book ${translateBook}`);
      res.send({book: translatedBook})
    }else{
      console.log("NOT found translated book");
      res.send({book: null})
    }
    
  }catch (error){
    res.send(error.msg)
  }
}

const getTranslatedBookText = async(req, res) => {
  const bookId = req.params.bookId;
  const language = req.params.language;
  try {
    const translatedBook = await TranslatedBook.findOne({ book_id: bookId, translated_language: language }, 'book_text');
    if (!translatedBook) {
      return res.status(404).json({ msg: 'Book not found' });
    }
    const bookText = translatedBook.book_text.toString('utf8');
    res.send(bookText);
    
  }catch (error){
    res.send(error.msg)
  }

}

function getLanguageName(languageCode) {
  const languageMap = {
    en: 'English',
    ro: 'Romanian',
    es: 'Spanish',
    fr: 'French',
    de: 'German',
    ar: 'Arabic',
    hi: 'Hindi',
    pt: 'Portuguese',
    ru: 'Russian',
    zh: 'Chinese',
    ja: 'Japanese',
    sq: 'Albanian',
    hy: 'Armenian',
    az: 'Azerbaijani',
    bn: 'Bengali',
    bs: 'Bosnian',
    bg: 'Bulgarian',
    ca: 'Catalan',
    hr: 'Croatian',
    cs: 'Czech',
    da: 'Danish',
    nl: 'Dutch',
    et: 'Estonian',
    fi: 'Finnish',
    ka: 'Georgian',
    el: 'Greek',
    gu: 'Gujarati',
    hu: 'Hungarian',
    id: 'Indonesian',
    it: 'Italian',
    ko: 'Korean',
    lv: 'Latvian',
    lt: 'Lithuanian',
    mk: 'Macedonian',
    ms: 'Malay',
    no: 'Norwegian',
    pl: 'Polish',
    sr: 'Serbian',
    sk: 'Slovak',
    sl: 'Slovenian',
    sv: 'Swedish',
    ta: 'Tamil',
    te: 'Telugu',
    th: 'Thai',
    tr: 'Turkish',
    uk: 'Ukrainian',
    ur: 'Urdu',
    vi: 'Vietnamese',
    cy: 'Welsh',
  };

  const languageName = languageMap[languageCode];

  if (languageName) {
    return languageName;
  } else {
    return 'Unknown Language';
  }
}

export {
  addBook, 
  updateBook, 
  deleteBook, 
  getAllBooks, 
  addBookToFavourites, 
  getBookIdText, 
  translateBook, 
  getBookById, 
  findTranslatedBook,
  getTranslatedBookText,
}