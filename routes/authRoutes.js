import express from 'express';
const router = express.Router();

import {
    register,
    login,
    updateUser,
    getFavourites,
    addFavourites,
    removeFromFavourites,
    addTranslatedToList,
    addBookToLibrary,
    addReadProgress,
} from '../controllers/authController.js'
import authenticateUser from '../middleware/auth.js'


router.post('/register', register)
router.post('/login', login)
router.route('/updateUser').patch(authenticateUser, updateUser)
router.route('/get-favourites').get(authenticateUser, getFavourites)
router.route('/add-favourites').patch(authenticateUser, addFavourites)
router.route('/remove-favourite-book').patch(authenticateUser, removeFromFavourites)
router.route('/add-translated').patch(authenticateUser, addTranslatedToList)
router.route('/add-to-library').patch(authenticateUser, addBookToLibrary)
router.route('/add-progress').patch(authenticateUser, addReadProgress)
export default router;