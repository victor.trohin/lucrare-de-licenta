import express from "express";
import connectDB from "./db/connect.js";
import 'express-async-errors'
import morgan from "morgan";
import { WebSocketServer } from 'ws';
import { createServer } from 'http';
//routers
import authRoute from './routes/authRoutes.js'
import bookRoute from "./routes/bookRoutes.js"

const app = express()
const server = createServer(app);
server.listen(8093);
const wss = new WebSocketServer({ server });

//for test
wss.on('connection', (ws) => {
    console.log('server: WebSocket connection established');
    ws.on('message', (message) => {
      console.log('server: Received message:', message);
    });
    ws.on('close', () => {
      console.log('server: WebSocket connection closed');
    });
  });

import dotenv from 'dotenv'
dotenv.config()

//midlleware
import notFoundMiddleware from "./middleware/not-found.js";
import errorHandlerMiddleware from "./middleware/error-handler.js";
import authenticateUser from './middleware/auth.js'

if(process.env.NODE_ENV !== 'production'){
    app.use(morgan('dev'))
}

app.use(express.json({ limit: '10mb', extended: true }));
app.use(express.urlencoded({ extended: true }));
app.get('/', (req, res) => {
    res.json({"lol": "lol"})
})

app.get('/api/v1', (req, res) => {
    res.json({"api": "v1"})
})

app.use('/api/v1/auth', authRoute)
app.use('/api/v1/books',authenticateUser, bookRoute)

app.use(notFoundMiddleware)
app.use(errorHandlerMiddleware)

const port = process.env.PORT || 5000

const start = async () => {
    try{
        await connectDB(process.env.MONGO_URL)
        app.listen(port, () => {
            console.log(`Server working on port ${port}...`)
        })
    }catch(error){
        console.log(error)
    }
}

start()

export {wss};