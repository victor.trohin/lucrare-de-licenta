import { 
    DISPLAY_ALERT,
    CLEAR_ALERT,
    REGISTER_USER_BEGIN,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_ERROR,
    LOGIN_USER_BEGIN,
    LOGIN_USER_ERROR,
    LOGIN_USER_SUCCESS,
    TOGGLE_SIDEBAR,
    USER_LOGOUT,
    UPDATE_USER_BEGIN,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_ERROR,
    GET_BOOKS_BEGIN,
    GET_ALL_BOOKS,
    ADD_FAVOURITES_BOOKS_BEGIN,
    ADD_FAVOURITES_BOOKS_SUCCESS,
    ADD_FAVOURITES_BOOKS_ERROR,
    REMOVE_FAVOURITES_BOOKS_BEGIN,
    REMOVE_FAVOURITES_BOOKS_SUCCESS,
    REMOVE_FAVOURITES_BOOKS_ERROR,
    GET_BOOKTEXT_BEGIN,
    GET_BOOKTEXT_SUCCESS,
    GET_BOOKTEXT_ERROR,
    ADD_TRANSLATED_TO_LIST_BEGIN,
    ADD_TRANSLATED_TO_LIST_SUCCESS,
    ADD_TRANSLATED_TO_LIST_ERROR,
    GET_TRANSLATED_TEXT_BEGIN,
    GET_TRANSLATED_TEXT_SUCCESS,
    GET_TRANSLATED_TEXT_ERROR,
    TRANSLATE_BOOK_BEGIN,
    TRANSLATE_BOOK_SUCCESS,
    TRANSLATE_BOOK_ERROR,
    TRANSLATE_BOOK_CONTINUE,
    ADD_BOOK_LIBRARY_BEGIN,
    ADD_BOOK_LIBRARY_SUCCESS,
    ADD_BOOK_LIBRARY_ERROR,
} from "./action"
import { initialState } from "./appContext"

const reducer = (state, action) => {
    if (action.type === DISPLAY_ALERT){
        return{
            ...state,
            showAlert: true,
            alertType: 'danger',
            alertText: 'Please provide all values!',
        }
    }
    
    if(action.type === CLEAR_ALERT){
        return{
            ...state,
            showAlert: false,
            alertType: '',
            alertText: '',
        }
    }
    if(action.type === REGISTER_USER_BEGIN){
        return{
            ...state,
            isLoading : true,
        }
    }
    if(action.type === REGISTER_USER_SUCCESS){
        return{
            ...state,
            isLoading : false,
            token: action.payload.token,
            user: action.payload.user,
            userLocation: action.payload.location,
            jobLocation: action.payload.location,
            showAlert: true,
            alertType: 'success',
            alertText: 'User created! Redirecting...',
        }
    }
    if(action.type === REGISTER_USER_ERROR){
        return{
            ...state,
            isLoading : false,
            showAlert: true,
            alertType: 'danger',
            alertText: action.payload.msg,
        }
    }
    if(action.type === LOGIN_USER_BEGIN){
        return{
            ...state,
            isLoading : true,
        }
    }
    if(action.type === LOGIN_USER_SUCCESS){
        return{
            ...state,
            isLoading : false,
            token: action.payload.token,
            user: action.payload.user,
            userLocation: action.payload.location,
            jobLocation: action.payload.location,
            favouritesList : action.payload.favourites_list,
            translatedList : action.payload.translated_list,
            showAlert: true,
            alertType: 'success',
            alertText: 'Login Successful! Redirecting...',
        }
    }
    if(action.type === LOGIN_USER_ERROR){
        return{
            ...state,
            isLoading : false,
            showAlert: true,
            alertType: 'danger',
            alertText: action.payload.msg,
        }
    }
    if(action.type === TOGGLE_SIDEBAR){
        return{
            ...state,
            showSidebar: !state.showSidebar,
        }
    }

    if(action.type === USER_LOGOUT){
        return{
            ...initialState,
            user: null,
            token: null,
            jobLocation: '',
            userLocation: '',
            favouritesList : [],
            translatedList : [],
        }
    }

    if (action.type === UPDATE_USER_BEGIN) {
        return { ...state, isLoading: true }
    }
    
    if (action.type === UPDATE_USER_SUCCESS) {
        return {
            ...state,
            isLoading: false,
            token: action.payload.token,
            user: action.payload.user,
            userLocation: action.payload.location,
            jobLocation: action.payload.location,
            showAlert: true,
            alertType: 'success',
            alertText: 'User Profile Updated!',
        }
    }
    if (action.type === UPDATE_USER_ERROR) {
        return {
            ...state,
            isLoading: false,
            showAlert: true,
            alertType: 'danger',
            alertText: action.payload.msg,
        }
    }
    if (action.type === GET_BOOKS_BEGIN) {
        return { ...state, isLoading: true, showAlert: false };
    }
    if (action.type === GET_ALL_BOOKS) {
        return {
            ...state,
            books: action.payload.books,
            isLoading: false,
        }
    }

    if(action.type === ADD_FAVOURITES_BOOKS_BEGIN){
        return{
            ...state,
            isLoading : true,
        }
    }
    if(action.type === ADD_FAVOURITES_BOOKS_SUCCESS){
        return{
            ...state,
            isLoading : false,
            favouritesList : action.payload.favourites_list,
            user: action.payload.updatedUser,
            showAlert: true,
            alertType: 'success',
            alertText: 'Book succesfuly added to favourites!',
        }
    }
    if(action.type === ADD_FAVOURITES_BOOKS_ERROR){
        return{
            ...state,
            isLoading : false,
            showAlert: true,
            alertType: 'danger',
            alertText: action.payload.msg,
        }
    }
    if(action.type === REMOVE_FAVOURITES_BOOKS_BEGIN){
        return{
            ...state,
            isLoading : true,
        }
    }
    if(action.type === REMOVE_FAVOURITES_BOOKS_SUCCESS){
        return{
            ...state,
            isLoading : false,
            favouritesList : action.payload.favourites_list,
            user: action.payload.updatedUser,
            showAlert: true,
            alertType: 'success',
            alertText: 'Book succesfuly removed from favourites!',
        }
    }
    if(action.type === REMOVE_FAVOURITES_BOOKS_ERROR){
        return{
            ...state,
            isLoading : false,
            showAlert: true,
            alertType: 'danger',
            alertText: action.payload.msg,
        }
    }

    if (action.type === GET_BOOKTEXT_BEGIN) {
        return { ...state, isLoading: true }
    }
    
    if (action.type === GET_BOOKTEXT_SUCCESS) {
        return {
            ...state,
            isLoading: false,
            showAlert: true,
            alertType: 'success',
            alertText: 'Success fetching the book!',
        }
    }
    if (action.type === GET_BOOKTEXT_ERROR) {
        return {
            ...state,
            isLoading: false,
            showAlert: true,
            alertType: 'danger',
            alertText: action.payload.msg,
        }
    }

    if (action.type === GET_TRANSLATED_TEXT_BEGIN) {
        return { ...state, isLoading: true }
    }
    
    if (action.type === GET_TRANSLATED_TEXT_SUCCESS) {
        return {
            ...state,
            isLoading: false,
            showAlert: true,
            alertType: 'success',
            alertText: 'Success fetching the translated book!',
        }
    }
    if (action.type === GET_TRANSLATED_TEXT_ERROR) {
        return {
            ...state,
            isLoading: false,
            showAlert: true,
            alertType: 'danger',
            alertText: action.payload.msg,
        }
    }

    if(action.type === ADD_TRANSLATED_TO_LIST_BEGIN){
        return{
            ...state,
            isLoading : true,
        }
    }
    if(action.type === ADD_TRANSLATED_TO_LIST_SUCCESS){
        return{
            ...state,
            isLoading : false,
            translatedList : action.payload.translated_list,
            user: action.payload.updatedUser,
            showAlert: true,
            alertType: 'success',
            alertText: 'Book succesfuly translated!',
        }
    }
    if(action.type === ADD_TRANSLATED_TO_LIST_ERROR){
        return{
            ...state,
            isLoading : false,
            showAlert: true,
            alertType: 'danger',
            alertText: action.payload.msg,
        }
    }

    if (action.type === TRANSLATE_BOOK_BEGIN) {
        return { 
            ...state, 
            translationIdInProgress: action.book_id,
        }
    }
    if (action.type === TRANSLATE_BOOK_CONTINUE) {
        return { 
            ...state, 
            progress: action.progress,
        }
    }
    
    if (action.type === TRANSLATE_BOOK_SUCCESS) {
        return {
            ...state,
            translationIdInProgress: null,
        }
    }
    if (action.type === TRANSLATE_BOOK_ERROR) {
        return {
            ...state,
            translationIdInProgress: null,
        }
    }

    if(action.type === ADD_BOOK_LIBRARY_BEGIN){
        return{
            ...state,
            isLoading : true,
        }
    }
    if(action.type === ADD_BOOK_LIBRARY_SUCCESS){
        return{
            ...state,
            isLoading : false,
            libraryList : action.payload.library_list,
            user: action.payload.updatedUser,
            showAlert: true,
            alertType: 'success',
            alertText: 'Book succesfuly added to library!',
        }
    }
    if(action.type === ADD_BOOK_LIBRARY_ERROR){
        return{
            ...state,
            isLoading : false,
            showAlert: true,
            alertType: 'danger',
            alertText: action.payload.msg,
        }
    }


    throw new Error(`no such action: ${action.type}`)
}

export default reducer