import React, { useEffect, useState } from 'react';
import { useAppContext } from '../../context/appContext';
import Wrapper from '../../assets/wrappers/BrowseBooks';
import LibraryBook from '../../components/LibraryBook';

const MyLibrary = () => {
  const { libraryList, books} = useAppContext();
  const [libraryBooks, setLibraryBooks] = useState([]);

  useEffect(() => {
    if (libraryList === null) {
      setLibraryBooks(null);
    } else {
      if (libraryList.length !== 0) {
        const updatedLibraryBooks = libraryList.map((libraryBook) => {
          const foundElement = books.find((book) => book.book_id === libraryBook.book_id);
          if (foundElement) {
            const isOriginalLanguage = foundElement.book_language === libraryBook.language;
            return {
              ...foundElement,
              book_language: libraryBook.language,
              isOriginalLanguage,
            };
          }
          return null;
        });
        setLibraryBooks(updatedLibraryBooks.filter(Boolean));
      }
    }
  }, [libraryList, books]);

  if (libraryBooks === null) {
    return <p>You have no books</p>;
  } else {
    return (
      <Wrapper>
        <div className="browse-books">
          <div className="book-grid">
            {libraryBooks.map((book, index) => (
              <LibraryBook key={index} book={book} />
            ))}
          </div>
        </div>
      </Wrapper>
    );
  }
}

export default MyLibrary