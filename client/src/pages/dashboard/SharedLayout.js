import React from 'react'
import { Outlet } from 'react-router-dom'
import Wrapper from '../../assets/wrappers/SharedLayout';
import {BigSlideBar} from '../../components'
import { SmallSlideBar} from '../../components'
import { NavBar} from '../../components'
const SharedLayout = () => {
  return (
    <Wrapper>
        <main className='dashboard'>
          <SmallSlideBar />
          <BigSlideBar />
          <div>
            <NavBar />
            <div className='dashboard-page'>
              <Outlet />
            </div>
          </div>
        </main>
      </Wrapper>
  )
}

export default SharedLayout