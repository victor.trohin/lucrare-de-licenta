import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useAppContext } from '../context/appContext';
import Wrapper from '../assets/wrappers/BookReader';


const BookReader = () => {
  const {id} = useParams();
  const [text, setText] = useState('');
  const [pages, setPages] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const { getBookText, isLoading, libraryList, addReadProgress } = useAppContext();
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      const response = await getBookText(id);
      const receivedText = response ? response : '';
      setText(receivedText);
    };

    fetchData();
    // eslint-disable-next-line
  }, [id]);

  useEffect(() => {
    const getProgress = () => {
      for (let i = 0; i < libraryList.length; i++) {
        const book = libraryList[i];
        if (book.book_id === id && book.language === 'en') {
          setProgress(Number(book.progress));
          setCurrentPage(Number(book.progress));
        }
      }
    };
  
    getProgress();
    // eslint-disable-next-line
  }, [libraryList, progress]);

  useEffect(() => {
    const splitTextIntoPages = () => {
      const linesPerPage = 20; 
      const lines = text.trim().split(/\r\n/);
      const pages = [];
      let page = '';
      let lineCount = 0;
      for (let i = 0; i < lines.length; i++) {
        const lineWords = lines[i].trim().split(" ");
        if(lineWords.length > 9){
          page += lines[i].replace(/\r\n/g, ' ') + ' ';
        }else{ 
          page += lines[i].replace(/\r\n/g, '<br>') + '<br>';
        }
        lineCount += 1;
        if (lineCount >= linesPerPage || i === lines.length - 1) {
          pages.push(page.trim());
          page = '';
          lineCount = 0;
        }
      }
      setPages(pages);
    };
    splitTextIntoPages();
  }, [text]);

  const goToNextPage = async () => {
    if (currentPage < pages.length - 1) {
      if(currentPage + 1 > progress)
        await addReadProgress({book_id: id, language: 'en', progress: 1})
      setCurrentPage(currentPage + 1);
    }
  };

  const goToPreviousPage = () => {
    if (currentPage > 0) {
      setCurrentPage(currentPage - 1);
    }
  };

  if (isLoading) return <p>Loading books...</p>;
  else
    return (
      <Wrapper>
      <div className='container'>
        <h1>Book Reader</h1>
        <div className="page-container">
          {/* {JSON.stringify(pages[currentPage])} */}
          <p dangerouslySetInnerHTML={{ __html: pages[currentPage] }}></p>
        </div>
          <p>
            Page {currentPage + 1} of {pages.length}
          </p>
        <div className='button-container'>
          <button className='btn btn-hero' onClick={goToPreviousPage} disabled={currentPage === 0}>
            Previous Page
          </button>
          <button className='btn btn-hero ' onClick={goToNextPage} disabled={currentPage === pages.length - 1}>
            Next Page
          </button>
        </div>
      </div>
    </Wrapper>
    );
};

export default BookReader;
