import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useAppContext } from '../context/appContext';
import Wrapper from '../assets/wrappers/BookReader';

const TranslatedBookReader = () => {
  const {id, language} = useParams();
  const [text, setText] = useState('');
  const [pages, setPages] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const {getTranslatedBookText, isLoading} = useAppContext();

  
  useEffect(() => {
    const fetchData = async () => {
      const response = await getTranslatedBookText({book_id: id, language: language});
      const receivedText = response ? response : '';
      setText(receivedText);
      console.log(receivedText)
    };
  
    fetchData();
    // eslint-disable-next-line
  }, [id]);

  useEffect(() => {
    const splitTextIntoPages = () => {
      const wordsPerPage = 200;
      const modifiedText = text.replace(/\n/g, ' <br> '); // Add spaces around newlines for proper splitting
      const words = modifiedText.trim().split(/\s+/);
      const pages = [];
      let currentPage = '';
    
      let currentWordCount = 0;
      let currentLineCount = 0;
    
      for (let i = 0; i < words.length; i++) {
        const word = words[i];
        currentWordCount++;
    
        if (word === '<br>') {
          currentLineCount++;
        }
    
        if (currentWordCount <= wordsPerPage && currentLineCount <= wordsPerPage) {
          currentPage += word + ' ';
        } else {
          pages.push(currentPage.trim());
          currentPage = '';
          currentWordCount = 0;
          currentLineCount = 0;
    
          if (word === '<br>') {
            currentLineCount++;
          }
    
          currentPage += word + ' ';
          currentWordCount++;
        }
      }
    
      if (currentPage.trim() !== '') {
        pages.push(currentPage.trim());
      }
    
      setPages(pages);
      setCurrentPage(0);
    };
    splitTextIntoPages();
  }, [text]);

  const goToNextPage = () => {
    if (currentPage < pages.length - 1) {
      setCurrentPage(currentPage + 1);
    }
  };

  const goToPreviousPage = () => {
    if (currentPage > 0) {
      setCurrentPage(currentPage - 1);
    }
  };

  if (isLoading) return <p>Loading books...</p>;
  else
    return (
      <Wrapper>
      <div className='container'>
        <h1>Book Reader</h1>
        <div className="page-container">
          {/* {JSON.stringify(pages[currentPage])} */}
          <p dangerouslySetInnerHTML={{ __html: pages[currentPage] }}></p>
        </div>
          <p>
            Page {currentPage + 1} of {pages.length}
          </p>
          <div className='button-container'>
          <button className='btn btn-hero' onClick={goToPreviousPage} disabled={currentPage === 0}>
            Previous Page
          </button>
          <button className='btn btn-hero ' onClick={goToNextPage} disabled={currentPage === pages.length - 1}>
            Next Page
          </button>
        </div>
      </div>
    </Wrapper>
    );
};

export default TranslatedBookReader;