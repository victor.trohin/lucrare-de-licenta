import React, { useState} from 'react';
import Wrapper from '../assets/wrappers/Book';
import { useAppContext } from '../context/appContext';
import LanguageMapper from '../utils/LanguageMapper';

function LibraryBook({ book }) {
  const {
    book_id,
    book_title,
    book_image,
    book_authors,
    book_language,
    book_shelves,
    isOriginalLanguage,
  } = book;
  const { progress } = useAppContext();
  const base64Data = btoa(String.fromCharCode.apply(null, new Uint8Array(book_image.data)));
  const imageDataUrl = `data:image/jpeg;base64,${base64Data}`;

  const handleReadButton = async () => {
    if (isOriginalLanguage) {
        window.open(`book/${book_id}`, '_blank');
    }
  };


  return (
    <Wrapper>
      <div className="book">
        <img src={imageDataUrl} alt={book_title} className="book-image" />
        <div className="book-details">
          <h2 className="book-title">{book_title}</h2>
          <p>
            <strong>Author:</strong> {book_authors}
          </p>
          <p>
            <strong>Bookshelves:</strong> {book_shelves}
          </p>
          <p><strong>Language:</strong> <LanguageMapper languageCode={book_language} /></p>
        </div>
        <div className="button-container">
            <button type="submit" className="btn btn-block" onClick={handleReadButton}>
                Read
            </button>
        </div>
      </div>
    </Wrapper>
  );
}

export default LibraryBook;
