import Wrapper from '../assets/wrappers/SmallSidebar';
import { IoArrowBackCircle } from 'react-icons/io5';
import { useAppContext } from '../context/appContext';
import NavLinks from './NavLinks';
import Logo from './Logo';


const SmallSlideBar = () => {
  const {showSidebar,toggleSideBar} = useAppContext()
  return (
    <Wrapper>
        <div className={showSidebar? "sidebar-container show-sidebar": "sidebar-container"}>
          <div className="content">
            <button
              type='button'
              className='close-btn'
              onClick={toggleSideBar}
            >
              <IoArrowBackCircle/>
            </button>
            <header>
              <Logo/>
            </header>
            <NavLinks toggleSideBar={toggleSideBar} />
          </div>
        </div>
    </Wrapper>
  )
}

export default SmallSlideBar