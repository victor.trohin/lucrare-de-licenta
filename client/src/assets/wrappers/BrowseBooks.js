import styled from 'styled-components'

const Wrapper = styled.div`
.browse-books {
  padding: 1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
}

.browse-books-title {
  font-size: 1.5rem;
  margin-bottom: 1rem;
}

.book-grid {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}
`

export default Wrapper
