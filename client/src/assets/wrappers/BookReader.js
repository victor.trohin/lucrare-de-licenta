import styled from 'styled-components';
import readerImage from '../images/reader.svg';

const Wrapper = styled.div`
    display: flex;
    
    background-size: cover;
body{

}
  .container {
    display: flex;
    align-items: center;
    flex-direction: column;
    /* background-image: url(${readerImage}); */
    background-size: cover;
  }

  .page-container {
    width: 550px; /* Adjust the maximum width as desired */
    height: 600px;
    padding: 20px;
  }

  p {
    font-family: "Bookman";
    font-size: 15px;
    line-height: 1.7;
    width: 500px;
    text-align: justify;
    word-wrap: break-word;
    display: inline-block;
  }

  .button-container {
    display: flex;
    gap: 10px;
  }
`;

export default Wrapper;
