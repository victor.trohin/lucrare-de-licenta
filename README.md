# MERN App

Welcome to Transli App! This is a user friendly web application from UI/UX point of view. The user will have a choice of a wide range of languages in which to translate the books made available. Additionally, users will be able to read translated books directly in the app, and the app will track reading progress. Thus, users will have a complete translation and reading experience in an easy-to-use and accessible environment. 

For the development of the application we used the MERN stack, a powerful and popular combination of technologies, which includes MongoDB as a database, Express.js for server management, React.js for the frontend and Node.js for the backend. This technology provides a scalable and efficient environment for developing modern web applications, ensuring a pleasant and high-performance experience for users.

## Features

- User authentication and authorization
- Full book translation from the provided list
- In-app book reading
- Integration with GPT-3.5 API from OpenAI
- Reading progress tracking
- Responsive and user-friendly UI

## Technologies Used

- MongoDB: A NoSQL database for data storage and retrieval.
- Express.js: A backend web application framework for handling server-side logic and API routes.
- React.js: A frontend JavaScript library for building user interfaces and managing application state.
- Node.js: A runtime environment for executing JavaScript code on the server-side.
- GPT OpenAI API - the model that will make the translation
- Guntenber.org platform as book provider

## Prerequisites

Before running the app, make sure you have the following prerequisites installed on your system:

- Node.js: [Download and Install Node.js](https://nodejs.org/en/download/)

## Installation

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/mern-app.git

2. Navigate to the project directory:

   ```bash
   cd mern-app

3. Install the dependencies:

  ```bash
  npm install

4.Start the development server:

  ```bash
  npm start


## Open the app
- The app will be available at http://localhost:3000.